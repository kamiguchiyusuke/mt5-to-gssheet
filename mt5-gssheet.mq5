#property copyright ""
#property link      ""
#property version   "1.0"
#property description "MT5からGoogleスプレッドシートにデータを出力します"
#property strict

#define OPEN_TYPE_PRECONFIG                0           // use the configuration by default
#define FLAG_KEEP_CONNECTION               0x00400000  // do not terminate the connection
#define FLAG_PRAGMA_NOCACHE                0x00000100  // no cashing of the page
#define FLAG_RELOAD                        0x80000000  // receive the page from the server when accessing it
#define SERVICE_HTTP                       3           // the required protocol
#define DEFAULT_HTTPS_PORT                 443
#define FLAG_SECURE                        0x00800000  // use PCT/SSL if applicable (HTTP)
#define INTERNET_FLAG_SECURE               0x00800000
#define INTERNET_FLAG_KEEP_CONNECTION      0x00400000
#define HTTP_ADDREQ_FLAG_REPLACE           0x80000000
#define HTTP_ADDREQ_FLAG_ADD               0x20000000

#import "wininet.dll"
int InternetOpenW(string sAgent,int lAccessType,string sProxyName,string sProxyBypass,int lFlags);
int InternetConnectW(int hInternet,string ServerName, int nServerPort,string lpszUsername, string lpszPassword, int dwService,int dwFlags,int dwContext);
int HttpOpenRequestW(int hConnect, string Verb, string ObjectName, string Version, string Referer, string AcceptTypes, int dwFlags, int dwContext);
int HttpSendRequestW(int hRequest, string &lpszHeaders, int dwHeadersLength, uchar &lpOptional[], int dwOptionalLength);
int InternetReadFile(int hFile,uchar &sBuffer[],int lNumBytesToRead,int &lNumberOfBytesRead);
int InternetCloseHandle(int hInet);
#import

string GAS_DEPLOY_ID = "";

int OnInit()
{
  if(MQLInfoInteger(MQL_DLLS_ALLOWED) != 1) {
    Print("==================");
    Print("DLL is not allowed");
    Print("==================");
    return(INIT_FAILED);
  }

  string post_data = "[[\"Hello \", \"World!!!!\"], [\"Good \", \"Morning\"]]";.// Sample data
  bool ret_call_gas = CallGoogleAppScript(post_data);
  return(INIT_SUCCEEDED);
}


void OnDeinit(const int reason)
{
  EventKillTimer();
}


void OnTick()
{
}


void OnTimer()
{
}


bool CallGoogleAppScript(string array_data){
  //+---------------------------------------+
  //|      GoogleAppScriptを呼び出す関数
  //+---------------------------------------+

  string sServerName = "script.google.com";
  string sObjectName = "/macros/s/" + GAS_DEPLOY_ID + "/exec";
  string headers = "";
  headers += "Content-Type: application/json\r\n";
  string dataString = "{\"data\":" + array_data + "}";
  int session = 0;
  int connect = 0;
  uchar data[];
  StringToCharArray(dataString, data, 0, -1, CP_UTF8);

  session = InternetOpenW("MetaTrader 5 Terminal" , 0, "", "", 0);
  connect = InternetConnectW(session, sServerName, DEFAULT_HTTPS_PORT, "", "", SERVICE_HTTP, 0, 0);
  int hRequest = HttpOpenRequestW(connect, "POST", sObjectName, "HTTP/1.1", NULL, NULL, (int)(FLAG_SECURE | FLAG_KEEP_CONNECTION | FLAG_RELOAD | FLAG_PRAGMA_NOCACHE), 0);
  int hSend = HttpSendRequestW(hRequest, headers, StringLen(headers), data, ArraySize(data) - 1);

  InternetCloseHandle(hSend);
  InternetCloseHandle(hRequest);

  return true;
}
