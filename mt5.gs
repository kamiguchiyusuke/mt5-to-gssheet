function doPost(e) {
  // POST データの JSON をパース
  var data = JSON.parse(e.postData.contents); 

  // 関数呼び出し
  writeToSheet(data.data);

  // レスポンス作成
  return ContentService.createTextOutput("Success");
}

function writeToSheet(dataArray) {
  // スプレッドシートの取得
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();

  // 最後の行を取得
  var lastRow = sheet.getLastRow();

  // 書き込む範囲を特定 (行番号, 列番号, 行数, 列数)
  var range = sheet.getRange(lastRow + 1, 1, dataArray.length, dataArray[0].length); 

  range.setValues(dataArray); // データを範囲にセット
}
